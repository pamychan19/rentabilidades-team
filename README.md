# Ultra bot plus

Ultra bot plus es un robot gratuito de código abierto que te permite generar dinero de distintos sitios web que pagan actualmente.

**Caracterícas:**
- El bot es 100% real, 100% configurable, se ejecuta en tu navegador.
- No requiere inversión (Agradecemos vuestras [donaciones](https://rentabilidadesweb.runkodapps.com/donaciones)).
- No daña economícamente a las webs (No se bloquea la publicidad).
- Es 100% configurable.
- Actualizaciones constantes.
- Comportamiento humano.

**Futuras caracteristicas:**
- Auto-acortadores de enlaces.
- Auto-captchas.

# Instalación paso a paso

1 - Instala el complemento "[Tampermonkey](https://www.tampermonkey.net/)" en tu navegador.

2 - Instala [nuestro script](https://universales.gitlab.io/rentabilidades-team/Ultra-Bot-Plus.user.js) en tu ordenador.

# Ejecución del bot

Tras realizar la instación, podrás ejecutar el bot desde la web [rentabilidadesweb.runkodapps.com](https://rentabilidadesweb.runkodapps.com/).

# Financiación

El proyecto se monetiza con la publicidad que se muestra en la web, si lo deseas, también puedes realizar [donaciones](https://rentabilidadesweb.runkodapps.com/donaciones).

